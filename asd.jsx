import React from "react";
import { useState } from 'react';

import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";


const From = () => {
  const [data, setData] = React.useState(null);

  const userData = {
    ...
  };

  const validationSchema = Yup.object({
...
    ),
  });

  return (
    <>
      <Formik
        initialValues={userData}
        validationSchema={validationSchema}
        validateOnChange={false}
        validateOnBlur={false}
        onSubmit={(values, { resetForm }) => {
          setData(values);
          alert(JSON.stringify(values, null, 2));
          resetForm();
        }}
      >
        {({ isValid, values, errors, touched, setFieldValue }) => {


const handleFirstNameChange = (event) => {
    setFieldValue("firstName", event.target.value);
    setData({ ...values, firstName: event.target.value });
  };
  
  

          return (
            
            <div class="mx-auto max-w-md border border-gray-300 p-4">
              <Form class="space-y-5">
                <div class="grid grid-cols-3 items-center">
                  <label htmlFor="" class="col-span-1 block text-left">
                    First Name
                  </label>
                  <div class="col-span-2">
                    <Field
                      name="firstName"
                      value={values.firstName}
                      onChange={handleFirstNameChange}
                      class={`block w-full rounded-md border ${
                        errors.firstName && touched.firstName
                          ? "border-red-500"
                          : "border-gray-300"
                      }`}
                    />
                    {errors.firstName && touched.firstName && (
                      <div className="text-red-500">{errors.firstName}</div>
                    )}
                  </div>
                </div>
                

                <div class="flex justify-center space-x-4 mt-8">
                  <button
                    type="submit"
                    class="bg-gradient-to-t from-blue-500 to-blue-300 hover:from-blue-700 hover:to-blue-400 text-white font-bold py-2 px-4 rounded"
                  >
                    Submit
                  </button>
                  <button
                    type="reset"
                    class="bg-gradient-to-t from-gray-400 to-gray-200 hover:from-gray-600 hover:to-gray-300 text-gray-700 font-bold py-2 px-4 rounded border border-gray-700 {{ empty($inputFields) ? 'text-white' : '' }}"
                  >
                    Reset
                  </button>
                </div>
                <div class="bg-gray-200 border border-gray-400 p-4">
                  {data && (
                    <div className="text-gray-800">
                      {JSON.stringify(data, null, 2)}
                    </div>
                  )}
                </div>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default From;
