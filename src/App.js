import './App.css';
import From from './components/From';

function App() {
  return (
    <div className="App">
      <From />
    </div>
  );
}

export default App;
