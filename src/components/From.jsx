import React from "react";
import { useState } from "react";


import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

const From = () => {
  const [data, setData] = React.useState(null);
  const [isDisabled, setIsDisabled] = useState(true);

  const userData = {
    firstName: "",
    lastName: "",
    age: 0,
    isEmployed: false,
    favoriteColor: [],
    sauces: [],
    stooge: "larry",
    notes: "",
  };

  const validationSchema = Yup.object({
    firstName: Yup.string()
      .required("First name is required")
      .matches(/^[A-Za-z ]+$/, "Only alphabet characters are allowed"),
    lastName: Yup.string()
      .required("Last name is required")
      .matches(/^[A-Za-z ]+$/, "Only alphabet characters are allowed"),
    age: Yup.number()
      .typeError("Age must be a number")
      .positive("Age must be a positive number")
      .integer("Age must be an integer"),
    notes: Yup.string().max(
      100,
      "Notes field should not exceed more than 100 characters"
    ),
  });

  return (
    <>
      <Formik
        initialValues={userData}
        validationSchema={validationSchema}
        validateOnChange={false}
        validateOnBlur={false}
        onSubmit={(values, { resetForm }) => {
          setData(values);
          alert(JSON.stringify(values, null, 2));
          resetForm();
          setIsDisabled(true);
        }}
        onReset={() => {
          setData(null);
          setIsDisabled(true);
        }}
      >
        {({ isValid, values, errors, touched, setFieldValue }) => {
          const handleFirstNameChange = (event) => {
            setFieldValue("firstName", event.target.value);
            setData({ ...values, firstName: event.target.value });
            setIsDisabled(false);
          };
          const handleLastNameChange = (event) => {
            setFieldValue("lastName", event.target.value);
            setData({ ...values, lastName: event.target.value });
            setIsDisabled(false);
          };
          const handleageChange = (event) => {
            setFieldValue("age", event.target.value);
            setData({ ...values, age: event.target.value });
            setIsDisabled(false);
          };
          const handleIsEmployedChange = (event) => {
            setFieldValue("isEmployed", event.target.checked);
            setData({ ...values, isEmployed: event.target.checked });
            setIsDisabled(false);
          };

          const handleFavoriteColorChange = (event) => {
            setFieldValue("favoriteColor", event.target.value);
            setData({ ...values, favoriteColor: event.target.value });
            setIsDisabled(false);
          };

          const handleSaucesChange = (event) => {
            const sauceValue = event.target.value;
            const isChecked = event.target.checked;

            let newSauces = [];

            if (isChecked) {
              newSauces = [...values.sauces, sauceValue];
            } else {
              newSauces = values.sauces.filter((sauce) => sauce !== sauceValue);
            }

            setFieldValue("sauces", newSauces);
            setData({ ...values, sauces: newSauces });
            setIsDisabled(false);
          };

          const handleStoogeChange = (event) => {
            const stoogeValue = event.target.value;

            setFieldValue("stooge", stoogeValue);
            setData({ ...values, stooge: stoogeValue });
            setIsDisabled(false);
          };

          const handleNotesChange = (event) => {
            setFieldValue("notes", event.target.value);
            setData({ ...values, notes: event.target.value });
            setIsDisabled(false);
          };

          return (
            <div class="mx-auto max-w-md border border-gray-300 p-4">
              <Form class="space-y-5">
                <div class="grid grid-cols-3 items-center">
                  <label htmlFor="" class="col-span-1 block text-left">
                    First Name
                  </label>
                  <div class="col-span-2">
                    <Field
                      name="firstName"
                      value={values.firstName}
                      onChange={handleFirstNameChange}
                      class={`block w-full rounded-md border ${
                        errors.firstName && touched.firstName
                          ? "border-red-500"
                          : "border-gray-300"
                      }`}
                    />
                    {errors.firstName && touched.firstName && (
                      <div className="text-red-500">{errors.firstName}</div>
                    )}
                  </div>
                </div>
                <div class="grid grid-cols-3 items-center">
                  <label htmlFor="" class="col-span-1 block text-left">
                    Last Name
                  </label>
                  <div class="col-span-2">
                    <Field
                      name="lastName"
                      value={values.lastName}
                      onChange={handleLastNameChange}
                      class={`block w-full rounded-md border ${
                        errors.lastName && touched.lastName
                          ? "border-red-500"
                          : "border-gray-300"
                      }`}
                    />
                    {errors.lastName && touched.lastName && (
                      <div class="text-red-500 text-sm">{errors.lastName}</div>
                    )}
                  </div>
                </div>
                <div class="grid grid-cols-3 items-center">
                  <label htmlFor="" class="col-span-1 block text-left">
                    Age
                  </label>
                  <div class="col-span-2">
                    <Field
                      name="age"
                      value={values.age}
                      onChange={handleageChange}
                      type="number"
                      class={`block w-full rounded-md border ${
                        errors.age && touched.age
                          ? "border-red-500"
                          : "border-gray-300"
                      }`}
                    />
                    {errors.age && touched.age && (
                      <div class="text-red-500 text-sm">{errors.age}</div>
                    )}
                  </div>
                </div>
                <div class="grid grid-cols-3 items-center">
                  <label htmlFor="" class="col-span-1 block text-left">
                    Is Employed
                  </label>
                  <div class="col-span-2 flex justify-center items-center">
                    <Field
                      name="isEmployed"
                      type="checkbox"
                      class="block rounded h-5 w-5 text-primary-600"
                      checked={values.isEmployed}
                      onChange={handleIsEmployedChange}
                    />
                  </div>
                </div>

                <div class="grid grid-cols-3 items-center">
                <label htmlFor="" class="col-span-1 block text-left">
                    Favorite Color
                </label>
                <div class="col-span-2">
                <Field
                  name="favoriteColor"
                  as={"select"}
                  class="block w-full rounded-md border border-gray-300"
                  onChange={handleFavoriteColorChange}
                >
                      <option value="">None</option>
                  <option value="red">
                    Red
                  </option>
                  <option value="blue">Blue</option>
                  <option value="green">Green</option>
                </Field>
                </div>
                </div>


                <div class="grid grid-cols-3 items-center">
                  <label htmlFor="" class="col-span-1 block text-left">
                    Sauces
                  </label>
                  <div class="col-span-2">
                    <div class="space-y-2">
                      <label for="ketchup" class="block text-left">
                        <Field
                          type="checkbox"
                          id="ketchup"
                          name="sauces"
                          value="ketchup"
                          checked={values.sauces.includes("ketchup")}
                          onChange={handleSaucesChange}
                          class="form-checkbox h-5 w-5 text-primary-600"
                        />
                        <span class="ml-2">Ketchup</span>
                      </label>
                      <label for="mustard" class="block text-left">
                        <Field
                          type="checkbox"
                          id="mustard"
                          name="sauces"
                          value="mustard"
                          checked={values.sauces.includes("mustard")}
                          onChange={handleSaucesChange}
                          class="form-checkbox h-5 w-5 text-primary-600"
                        />
                        <span class="ml-2">Mustard</span>
                      </label>
                      <label for="mayonnaise" class="block text-left">
                        <Field
                          type="checkbox"
                          id="mayonnaise"
                          name="sauces"
                          value="mayonnaise"
                          checked={values.sauces.includes("mayonnaise")}
                          onChange={handleSaucesChange}
                          class="form-checkbox h-5 w-5 text-primary-600"
                        />
                        <span class="ml-2">Mayonnaise</span>
                      </label>
                      <label for="guacamole" class="block text-left">
                        <Field
                          type="checkbox"
                          id="guacamole"
                          name="sauces"
                          value="guacamole"
                          checked={values.sauces.includes("guacamole")}
                          onChange={handleSaucesChange}
                          class="form-checkbox h-5 w-5 text-primary-600"
                        />
                        <span class="ml-2">Guacamole</span>
                      </label>
                    </div>
                  </div>
                </div>

                <div class="grid grid-cols-3 items-center">
                  <label htmlFor="" class="col-span-1 block text-left">
                    Stooge
                  </label>
                  <div class="col-span-2">
                    <div class="space-y-2">
                      <label for="stooge_larry" class="block text-left">
                        <Field
                          id="stooge_larry"
                          name="stooge"
                          type="radio"
                          value="larry"
                          onChange={handleStoogeChange}
                          class="form-radio h-4 w-4 text-primary-600 transition duration-150 ease-in-out"
                        />
                        <span class="ml-2">Larry</span>
                      </label>
                      <label for="stooge_moe" class="block text-left">
                        <Field
                          id="stooge_moe"
                          name="stooge"
                          type="radio"
                          value="moe"
                          onChange={handleStoogeChange}
                          class="form-radio h-4 w-4 text-primary-600 transition duration-150 ease-in-out"
                        />
                        <span class="ml-2">Moe</span>
                      </label>
                      <label for="stooge_curly" class="block text-left">
                        <Field
                          id="stooge_curly"
                          name="stooge"
                          type="radio"
                          value="curly"
                          onChange={handleStoogeChange}
                          class="form-radio h-4 w-4 text-primary-600 transition duration-150 ease-in-out"
                        />
                        <span class="ml-2">Curly</span>
                      </label>
                    </div>
                  </div>
                </div>

                <div class="grid grid-cols-3 items-center">
                  <label htmlFor="" class="col-span-1 block text-left">
                    Notes
                  </label>
                  <div class="col-span-2">
                    <Field
                      name="notes"
                      as="textarea"
                      onChange={handleNotesChange}
                      value={values.notes}
                      className={`block w-full rounded-md border ${
                        errors.notes && touched.notes
                          ? "border-red-500"
                          : "border-gray-300"
                      }`}
                    />
                    {errors.notes && touched.notes && (
                      <div className="text-red-500">{errors.notes}</div>
                    )}
                  </div>
                </div>

                <div class="flex justify-center space-x-4 mt-8">
                <button
  type="submit"
  disabled={isDisabled}
  // disabled={!isValid}
  class={`bg-gradient-to-t from-blue-500 to-blue-300 hover:from-blue-700 hover:to-blue-400 text-white font-bold py-2 px-4 rounded ${isDisabled ? 'opacity-50' : ''}`}
>
  Submit
</button>

<button
  type="reset"
  disabled={isDisabled}
  // disabled={!isValid}
  class={`bg-gradient-to-t from-gray-400 to-gray-200 hover:from-gray-600 hover:to-gray-300 text-gray-700 font-bold py-2 px-4 rounded border border-gray-700 ${isDisabled ? 'opacity-50' : ''}`}
>
  Reset
</button>
                </div>
                <div class="bg-gray-200 border border-gray-400 p-4">
                  {data && (
                    <div className="text-gray-800">
                      {JSON.stringify(data, null, 2)}
                    </div>
                  )}
                </div>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default From;
